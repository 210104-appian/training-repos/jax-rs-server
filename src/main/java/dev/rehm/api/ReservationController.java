package dev.rehm.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dev.rehm.models.Reservation;
import dev.rehm.services.ReservationService;

/*
 * GET /reservations -> get all reservations
 * GET /reservations/{id} -> get one reservation by its ID
 * GET /reservations?customer={customerName} -> get reservations based on customer 
 * POST /reservations -> add a new reservation
 * DELETE /reservations/{id} -> delete reservation by ID
 * PUT /reservations/{id} -> complete update of reservation by ID 
 * PATCH /reservation/{id} -> partial update of reservation by ID 
 * 
 */

@Path("/reservations")
public class ReservationController {
	
	private ReservationService rService = new ReservationService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Reservation> getAllReservations(@QueryParam("customer")String customer){
		System.out.println(customer);
		if(customer==null) {
			return rService.getAll();			
		} 
		return rService.getReservationByCustomerName(customer);
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReservationById(@PathParam("id") int id) {
		Reservation reservation = rService.getReservationById(id);
		if(reservation==null) {
			return Response.status(404).build();
		} else {
			return Response.status(200).entity(reservation).build();
		}
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewUser(Reservation reservation) {
		boolean success = rService.createNewReservation(reservation);
		if(success) {
			return Response.status(201).entity(reservation).build();
		}
		return Response.status(400).build();
	}
	
}
